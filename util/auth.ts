import { auth, db } from '../config/firebase';

export const signup = async (email, password) => {
    try {
        const res = await auth.createUserWithEmailAndPassword(email, password);
        return res
    } catch (err) {
        console.log('signup', err);
    }
}
export const createUserDB = async (user) => {
    if (!user) {
        throw Error('no user Detected')
    }
    try {
        const res = await db.collection('users').doc(user.id).set(user);
        return res

    } catch (err) {
        console.log('createUserDB', err);
    }
}


