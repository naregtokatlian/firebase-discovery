import Head from 'next/head'
import Link from 'next/link'
import { Flex, FormControl, Input, Stack, Button, Link as cLink } from '@chakra-ui/react'
import { useForm } from 'react-hook-form'
import { useRequireAuth } from '../../hooks/useRequireAuth'
import { useRouter } from 'next/router'

interface IUserInput {
    email: string;
    password: string
}

const Login = () => {
    const router = useRouter();
    const auth = useRequireAuth();
    const { register, errors, handleSubmit, formState } = useForm<IUserInput>();
    const onSubmit = async (data: IUserInput) => {
        await auth.signin(data.email, data.password);
        router.push('/')
    }
    const handleFBLogin = async () => {
        await auth.signupWithFB();
        router.push('/')
    }
    const handleGoogleLogin = async () => {
        await auth.signupWithGoogle();
        router.push('/')
    }

    return (
        <>
            <Head>
                <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
            </Head>
            <Flex>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl>
                        <Stack>
                            <Input
                                name="email"
                                placeholder="email"
                                ref={register({
                                    required: 'Please enter your email'
                                })}
                            />
                            {errors.email &&
                                <span style={{ color: "red" }}>{errors.email.message}</span>}
                            <Input
                                type="password"
                                name="password"
                                placeholder="password"
                                ref={register({
                                    required: 'Please enter your password'
                                })}
                            />
                            {errors.password &&
                                <span style={{ color: "red" }} >{errors.password.message}</span>}
                            <Link href='/passwordReset'>
                                <a>
                                    forgot you <span style={{ color: "blue" }}>Password</span>?
                                </a>
                            </Link>
                            <Link href='/signup'>
                                <a>
                                    Don't have an account <span style={{ color: "blue" }}>signup</span>?
                                </a>
                            </Link>
                            <Button type="submit" colorScheme="green" isLoading={formState.isSubmitting}>
                                Login
                        </Button>
                            <Button onClick={handleGoogleLogin} colorScheme='red'>
                                <i className="fab fa-google"></i>
                            </Button>
                            <Button colorScheme='blue' onClick={handleFBLogin}>
                                <i className="fab fa-facebook-square"></i>
                            </Button>
                        </Stack>
                    </FormControl>
                </form>
            </Flex>
        </>
    )
}

export default Login
