import React from 'react'
import { Button } from '@chakra-ui/react'
import { useAuth } from '../../hooks/useAuth'

const SignOut = () => {
    const { signout } = useAuth()
    return (
        <div>
            <Button onClick={signout}>SignOut</Button>
        </div>
    )
}

export default SignOut
