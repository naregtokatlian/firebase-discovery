import React from 'react'
import { useForm } from 'react-hook-form'
import { useAuth } from '../../hooks/useAuth'
import { Input, VStack, Button, Center, FormControl } from '@chakra-ui/react'
import { useRouter } from 'next/router'
import Head from 'next/head'


const Signup = () => {
    interface userInput {
        email: string;
        password: string;
    }
    const router = useRouter();
    const auth = useAuth();
    const { register, errors, handleSubmit, formState } = useForm<userInput>();

    const onSubmit = async (data: userInput) => {
        try {
            await auth.signup(data.email, data.password)
            router.push('/')
        } catch (error) {
            console.log(error);
        }
    }
    const googleSignup = async () => {
        try {
            const { user } = await auth.signupWithGoogle();
            router.push('/')
        } catch (error) {
            console.log('googleSignup error:', error);
        }
    }
    return (
        <>
            <Head>
                <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
            </Head>
            <form onSubmit={handleSubmit(onSubmit)}>
                <FormControl isInvalid={errors.email ? true : errors.password ? true : false} onSubmit={handleSubmit(onSubmit)}>
                    <VStack className="faddass" background="green" width='100%' spacing="12px">
                        <Input
                            placeholder="Email"
                            type="email"
                            name="email"
                            ref={register({
                                required: 'Please enter you email'
                            })}
                        />
                        {errors.email && (
                            <span>{errors.email.message}</span>
                        )
                        }
                        <Input
                            placeholder="Password"
                            type="password"
                            name="password"
                            ref={register({
                                required: 'Please enter you password'
                            })}
                        />
                        {errors.password &&
                            <span>{errors.password.message}</span>
                        }

                        <Center>
                            <Button colorScheme="green" isLoading={formState.isSubmitting} variant="solid" type="submit">Sign in</Button>
                        </Center>
                        <Button onClick={googleSignup}>
                            <i className="fab fa-google"></i>
                        </Button>
                    </VStack >
                </FormControl>
            </form>
        </>
    )
}
export default Signup