import { useState, useContext, createContext, useEffect } from 'react';
import { db, auth, googleProvider, fbProvider } from '../config/firebase'
import firebase from 'firebase'

const authContext = createContext({ user: {} });
const { Provider } = authContext;

interface iDbUser {
    uid: string;
    displayName: string;
    email: string;
    photo: string
}

export const AuthProvider = ({ children }): JSX.Element => {
    const auth = useAuthProvider();
    return <Provider value={auth} >{children}</Provider>
}

export const useAuth: any = () => {
    return useContext(authContext);
}

const useAuthProvider = () => {
    const [user, setUser] = useState(null);

    useEffect(() => {
        if (user?.uid) {
            const unsubscribe = async () => {
                db.collection('users')
                    .doc(user.uid)
                    .onSnapshot((doc) => setUser(doc.data()));
            }
            unsubscribe();
        }
    }, [])

    const handleAuthStateChange = (user: firebase.User) => {
        setUser(user);
        if (user) {
            getUserAdditionalData(user)
        }
    };

    useEffect(() => {
        const unsub = auth.onAuthStateChanged(handleAuthStateChange);
        return () => unsub();
    }, [])

    // db helpers
    const createUserDB = async (user: firebase.User) => {
        if (!user) {
            throw Error('no user Detected')
        }
        const data: iDbUser = {
            displayName: user.displayName,
            email: user.email,
            uid: user.uid,
            photo: user.photoURL
        }
        try {
            const res = await db.collection('users').doc(user.uid).set(data);
            setUser(data)
            return res
        } catch (err) {
            console.log('createUserDB', err);
        }
    }

    const isUserNew = async (id: string) => {
        const res = await db.collection('users').doc(id).get();
        return res.exists
    }

    const getUserAdditionalData = async (user: firebase.User) => {
        try {
            const userData = await db.collection('users')
                .doc(user.uid)
                .get();
            if (userData.data()) {
                setUser(userData.data())
            }
        } catch (error) {
            console.log('getUserAddtionalData error:', error);
        }
    };

    //user actions

    const signup = async (email: string, password: string) => {
        try {
            const res = await auth.createUserWithEmailAndPassword(email, password);
            createUserDB(res.user)
            return res
        } catch (err) {
            console.log('signup', err);
        }
    }

    const signin = async (email: string, password: string) => {
        try {
            const res = await auth.signInWithEmailAndPassword(email, password)
            setUser(res.user)
            getUserAdditionalData(user)
            return res
        } catch (error) {
            console.log('signin error', error);
        }
    }

    const signout = async () => {
        try {
            const res = await auth.signOut();
            setUser(false);
            return res
        } catch (error) {
            console.log('signout error:', error);
        }
    }
    const passwordReset = async (email: string) => {
        try {
            const res = await auth.sendPasswordResetEmail(email);
            return res
        } catch (error) {
            console.log('passwordReset error:', error);
        }
    }
    const signupWithGoogle = async () => {
        try {
            const res = await auth.signInWithPopup(googleProvider)

            if (isUserNew(res.user.uid)) {
                createUserDB(res.user)
            } else {
                getUserAdditionalData(res.user)
            }
            return res
        } catch (error) {
            console.log('signupWithGoogle error:', error);
        }
    }
    const signupWithFB = async () => {
        try {
            const res = await auth.signInWithPopup(fbProvider)
            if (res) {
                console.log('fb data: ', res.user.displayName);
            }
            return res
        } catch (error) {
            console.log('signupWithFB error: ', error);
        }
    }
    return { user, signup, signin, signout, passwordReset, signupWithGoogle, signupWithFB }
}