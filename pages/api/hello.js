// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { run } from '../../migration/migrate'
export default (req, res) => {
  run();
  res.statusCode = 200
  res.json({ name: 'John Doe' })
}
