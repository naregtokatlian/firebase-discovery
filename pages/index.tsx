import SignOut from '../components/auth/SignOut';
import { useRequireAuth } from '../hooks/useRequireAuth';


export default function Home() {
  const { user } = useRequireAuth();
  console.log('logged in user', user);

  return (
    <div>
      <h1>HomePage</h1>
      {user &&
        (
          <div>
            <h1>Welcome back {user.displayName}</h1>
            <SignOut />
            <p>this is a very secret info that u can only access it when u login</p>
          </div>


        )}

    </div>
  )

}
