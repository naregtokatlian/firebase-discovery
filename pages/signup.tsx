import React from 'react'
import Signup from '../components/auth/Signup'
import { Box, Center } from '@chakra-ui/react'
const signupPage = () => {

    return (
        <Center>
            <Box maxW="500px" minW="350px">
                <Signup />
            </Box>
        </Center>
    )
}

export default signupPage
