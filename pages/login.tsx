import { Flex, Box } from '@chakra-ui/react'
import Login from '../components/auth/Login'
import Head from 'next/head'
const login = () => {

    return (
        <>
            <Head>
                <link rel="stylesheet"
                    href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
            </Head>
            <Flex className='nareg' h="80vh" justifyContent='center' alignItems='center'>
                <Box w='30%' display='flex' justifyContent='center' >
                    <Login />
                </Box>
            </Flex>
        </>
    )
}

export default login
