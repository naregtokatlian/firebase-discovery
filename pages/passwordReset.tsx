import { useForm } from 'react-hook-form'
import { useAuth } from '../hooks/useAuth'
import { Flex, Stack, FormControl, Input, Button } from '@chakra-ui/react'
import { useRouter } from 'next/router'

const passwordReset = () => {
    const router = useRouter();
    const auth = useAuth();
    const { register, errors, handleSubmit, formState } = useForm();
    const onSubmit = ({ email }) => {
        auth.passwordReset(email)
        router.push('/login')
    }
    return (
        <>
            <Flex>
                <form onSubmit={handleSubmit(onSubmit)} >
                    <FormControl isInvalid={errors.email}>
                        <Stack>
                            <Input
                                name="email"
                                type="email"
                                placeholder="email"
                                ref={register({
                                    required: 'Please enter your email',
                                })}
                            />
                            {errors.email && (
                                <span>{errors.email.message}</span>
                            )}
                            <Button
                                type="submit"
                                isLoading={formState.isSubmitting}
                                colorScheme="green"
                            >
                                Reset</Button>
                        </Stack>
                    </FormControl>
                </form>
            </Flex>
        </>

    )
}

export default passwordReset
